package main

import (
	"log"
	"net/http"
)

func serverMain() error {
	log.Printf("Starting wg-auth-proxy...")

	server.WGServer.PublicKey = getWGServerPubkey()

	// Generates necessary routes
	router := NewRouter()

	// Start server
	log.Fatal(http.ListenAndServe(":80", router))
	return nil
}
