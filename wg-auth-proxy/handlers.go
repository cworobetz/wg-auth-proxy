package main

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"os/exec"
	"time"

	"github.com/golang/gddo/httputil/header"
)

// Auth (/api/auth) takes json with valid API key and returns a WGAuthResp object
func auth(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "" {
		value, _ := header.ParseValueAndParams(r.Header, "Content-Type")
		if value != "application/json" {
			msg := "Content-Type header is not application/json"
			http.Error(w, msg, http.StatusUnsupportedMediaType)
			return
		}
	}

	// Use http.MaxBytesReader to enforce a maximum read of 1MB from the
	// response body. A request body larger than that will now result in
	// Decode() returning a "http: request body too large" error.
	r.Body = http.MaxBytesReader(w, r.Body, 1048576)

	// If it fails decoding according to the object passed, error out
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	var authRecv WGAuthRecv

	err := decodeJSONBody(w, r, &authRecv)
	if err != nil {
		var malreq *malformedRequest
		if errors.As(err, &malreq) {
			http.Error(w, malreq.msg, malreq.status)
		} else {
			log.Println(err.Error())
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		return
	}

	if authRecv.APIKey != server.APIKey {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		log.Printf("Authentication denied. API key is not valid.")
	}

	peer := WGPeerConfig{PublicKey: authRecv.PublicKey, AllowedIPs: authRecv.AllowedIPs}
	clientConf := ClientState{Config: &peer}

	log.Printf("Authentication request received for peer %s", peer.PublicKey)

	// Check to see if peer already present. If so, extend the duration and return
	for _, client := range server.WGClients {
		if peer.PublicKey == client.Config.PublicKey {
			// Client is already present, so extend their current duration
			client.Timer.Stop()
			client.Timer.Reset(time.Duration(authRecv.TimeoutSec) * time.Second)

			// Generate response obj
			resp := WGAuthResp{Status: "OK", PublicKey: client.Config.PublicKey, AllowedIPs: client.Config.AllowedIPs, TimeoutSec: authRecv.TimeoutSec}

			json.NewEncoder(w).Encode(resp)
			log.Printf("Successfully reupped peer %s with AllowedIPs %s for %d seconds", peer.PublicKey, peer.AllowedIPs, authRecv.TimeoutSec)
			return
		}
	}

	// Add the client as a peer to the wireguard server
	cmd := exec.Command("wg", "set", "wg0", "peer", clientConf.Config.PublicKey, "allowed-ips", clientConf.Config.AllowedIPs)
	_, err = cmd.CombinedOutput()
	if err != nil {
		log.Fatalf("Error adding WireGuard client: %v", err)
	}

	// Set a timer to remove peer at timeout
	timer := time.NewTimer(time.Duration(authRecv.TimeoutSec) * time.Second)
	newClient := ClientState{Config: &peer, Timer: timer}
	server.WGClients = append(server.WGClients, newClient)

	go func(client ClientState) {
		<-timer.C
		log.Printf("Removing expired for client %s", client.Config.PublicKey)
		cmd = exec.Command("wg", "set", "wg0", "peer", client.Config.PublicKey, "remove")
		_, err = cmd.CombinedOutput()
		if err != nil {
			log.Fatalf("Error removing WireGuard peer: %v", err)
		}
		// Remove client from server
		for i, wgClient := range server.WGClients {
			if client.Config.PublicKey == wgClient.Config.PublicKey {
				server.WGClients[i] = server.WGClients[len(server.WGClients)-1]
				server.WGClients[len(server.WGClients)-1] = ClientState{}
				server.WGClients = server.WGClients[:len(server.WGClients)-1]
			}
		}
	}(newClient)

	// Generate response obj
	resp := WGAuthResp{Status: "OK", PublicKey: newClient.Config.PublicKey, AllowedIPs: newClient.Config.AllowedIPs, TimeoutSec: authRecv.TimeoutSec}
	json.NewEncoder(w).Encode(resp)

	log.Printf("Successfully added peer %s with AllowedIPs %s for %d seconds", clientConf.Config.PublicKey, clientConf.Config.AllowedIPs, authRecv.TimeoutSec)
}
