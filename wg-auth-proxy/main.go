package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
)

var server *ServerState = new(ServerState)

func main() {

	// Handles assigning flag vars
	flags()

	// Start the main server
	err := serverMain()
	if err != nil {
		log.Fatalf("Fatal server error: %v", err)
	}
}

func flags() {

	// Declare flags
	flag.StringVar(&server.APIKey, "apikey", os.Getenv("WG_AUTH_PROXY_APIKEY"), "The API key to authenticate to the server. Defaults to $WG_AUTH_PROXY_APIKEY")               // Global, for ease of access elsewhere
	flag.StringVar(&server.WGServer.Endpoint, "endpoint", os.Getenv("WG_SERVER_ENDPOINT"), "The WireGuard server's endpoint, e.g. vpn.mydomain.com:51820 or 10.0.0.40:51820") // Global, for ease of access elsewhere
	genkey := flag.Bool("genkey", false, "Generates an API key, prints it to stdout and exits.")

	flag.Parse()

	// -genkey
	if *genkey {
		cmd := exec.Command("wg", "genkey")
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf(string(out))
		os.Exit(0)
	}

	// -apikey
	if len(server.APIKey) == 0 {
		log.Fatalf("You need to specify \"-apikey\". Use \"wg-auth-proxy --help\" for more information.")
	}

	// -endpoint
	if len(server.WGServer.Endpoint) == 0 {
		log.Fatalf("You need to specify \"-endpoint\" . Use \"wg-auth-proxy --help\" for more information")
	}
}
