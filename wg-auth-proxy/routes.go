package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

// Route contains all the information needed to create an API route
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes is an array of type routes
type Routes []Route

// NewRouter create routes in the HTTP server for all passed in Route types
func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

var routes = Routes{
	Route{
		"auth",
		"POST",
		"/api/auth",
		auth,
	},
}
