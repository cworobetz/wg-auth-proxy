package main

// WGAuthRecv contains all the information required for a valid authentication request to wg-auth-proxy
type WGAuthRecv struct {
	APIKey     string `json:"apikey"`
	PublicKey  string `json:"publickey"`
	AllowedIPs string `json:"allowedips"`
	TimeoutSec int    `json:"timeoutsec"`
}

// WGAuthResp contains all the information required for a valid authentication request to wg-auth-proxy
type WGAuthResp struct {
	Status     string `json:"status"`
	PublicKey  string `json:"publickey"`
	AllowedIPs string `json:"allowedips"`
	TimeoutSec int    `json:"timeoutsec"`
}
