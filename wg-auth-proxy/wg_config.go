package main

import (
	"log"
	"os/exec"
	"time"
)

// WGServerConfig contains all the necessary information about the server's configuration
type WGServerConfig struct {
	Endpoint   string
	PublicKey  string
	AllowedIPs string
}

// WGPeerConfig contains the configuration for a single wireguard peer
type WGPeerConfig struct {
	PublicKey  string
	AllowedIPs string
}

// ClientState contains all currently active client sessions, including their peer info and their duration timer
type ClientState struct {
	Config *WGPeerConfig
	Timer  *time.Timer
}

// ServerState contains the servers current configuration and the state of all clients
type ServerState struct {
	WGServer  WGServerConfig
	WGClients []ClientState
	APIKey    string
}

func getWGServerPubkey() string {

	// Get publickey
	cmd := exec.Command("wg", "show", "wg0", "public-key")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	return string(out)
}
