# alpine:3.11.5
# Installs wireguard-tools so that it may be cached for later builds
FROM alpine@sha256:afb25d7b78d81dd0166c9f510bfa7a1f7071fdecd5fe846ebfb2a6d691828c05 as alpinecache
RUN apk --no-cache add -U wireguard-tools

# golang:1.14.2
FROM golang@sha256:9100c9f7d9924cced40f9ff3872b88334ad26a20685564988aefd2d88f6e6c75 as go
WORKDIR /app
# Download dependencies first
COPY go.mod .
COPY go.sum .
RUN go mod download
# Build the binary
RUN ls -lah
COPY ./wg-auth-proxy /app
RUN CGO_ENABLED=0 GOOS=linux go build -o /go/bin/wg-auth-proxy .

# alpinecache, from the first stage
FROM alpinecache as alpine
WORKDIR /app
# Copy over the binary
COPY --from=go /go/bin/wg-auth-proxy /app
CMD ["./wg-auth-proxy"]
EXPOSE 80
