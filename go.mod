module gitlab.com/cworobetz/wg-auth-proxy

go 1.12

require (
	github.com/golang/gddo v0.0.0-20200324184333-3c2cc9a6329d
	github.com/gorilla/mux v1.7.4
)
